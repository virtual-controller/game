extends RigidBody2D

signal was_hit
var LAST_INPUT = {'joystick': {'x': 0, 'y': 0},
	'a_button': {'mode': 'Up'},
	'b_button': {'mode': 'Up'},
	'command_button': {'mode': 'Up'},
}

func hit():
	was_hit.emit()

func set_color(color):
	$Polygon2D.color = color

func set_last_input(input):
	LAST_INPUT = input

func _integrate_forces(state):
	var joystick_force = Vector2.ZERO
	var x = LAST_INPUT['joystick']['x']
	var y = LAST_INPUT['joystick']['y']
	joystick_force.x = float(x)/100.0
	joystick_force.y = float(-y)/100.0
	if joystick_force.length() > 0:
		joystick_force *= 20.0
	state.apply_force(joystick_force)
	
		# Rotate the player in the direction of the joystick
	if joystick_force.length() > 0.0:
		var joy_rot = joystick_force.angle()
		state.transform = Transform2D(joy_rot, position)


	# Drag force
	var drag_force = Vector2.ZERO
	var vel = state.linear_velocity
	drag_force.x = clamp(0.001*pow(vel.x, 2), -100, 100)
	if vel.x > 0:
		drag_force.x *= -1
	drag_force.y = clamp(0.001*pow(vel.y, 2), -100, 100)
	if vel.y > 0:
		drag_force.y *= -1
	state.apply_force(drag_force)
