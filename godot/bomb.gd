extends CharacterBody2D

signal pickup

var SPEED = 500.0
var IS_EXPLODING = false
var EXPLOSION_SPRITE = null
var EXPLOSION_FADE_TIME = 2
var IS_ITEM = false

func hit():
	explode(null)

func spawn_item_in_area(area):
	var spawnArea = area.shape.extents
	var origin = area.global_position
	var x = randf_range(origin.x - spawnArea.x, origin.x + spawnArea.x)
	var y = randf_range(origin.y - spawnArea.y, origin.y + spawnArea.y)
	var pos = Vector2(x, y)
	spawn_item(pos)

func spawn_item(pos):
	SPEED = 0
	IS_ITEM = true
	position = pos

func start(_position, _direction, player_speed):
	rotation = _direction
	position = _position
	velocity = Vector2(SPEED, 0).rotated(rotation) + player_speed

func explode(collision):
	if IS_EXPLODING == false:
		IS_EXPLODING = true
		SPEED = 0
		velocity = Vector2.ZERO
		$ExplosionTimer.wait_time = EXPLOSION_FADE_TIME
		$ExplosionTimer.start()
		
		$Sprite2D.visible = false
		
		# Explosion texture
		var explosition_sprite = Sprite2D.new()
		var texture = GradientTexture2D.new()
		texture.fill = GradientTexture2D.FILL_RADIAL
		texture.fill_from = Vector2(0.5, 0.5)
		texture.fill_to = Vector2(1, 0.5)
		texture.width = 250
		texture.height = 250
		var gradient = Gradient.new()
		var white = Color.WHITE
		var invis = Color(1,1,1,0)
		# XXX There are default ones apparently
		gradient.set_color(0, white)
		gradient.set_color(1, invis)
		texture.gradient = gradient
		explosition_sprite.texture = texture
		EXPLOSION_SPRITE = explosition_sprite
		
		var area = Area2D.new()
		var col = CollisionShape2D.new()
		var shape = CircleShape2D.new()
		shape.radius = 125
		col.shape = shape
		area.add_child(col)
		area.add_child(explosition_sprite)
		area.body_entered.connect(on_body_entered_explosion_area)
		add_child(area)

func _process(delta):
	if IS_EXPLODING:
		var fade_percent = $ExplosionTimer.time_left / EXPLOSION_FADE_TIME
		var color = Color(1,1,1, fade_percent)
		EXPLOSION_SPRITE.texture.gradient.set_color(0, color)

func _physics_process(delta):
	var collision = move_and_collide(velocity*delta)
	if collision:
		if IS_ITEM == true && collision.get_collider().has_method("pickup_item"):
			collision.get_collider().pickup_item("BOMB")
			queue_free()
		elif IS_EXPLODING == false:
			explode(collision)
		if IS_ITEM == false && collision.get_collider().has_method("hit"):
			collision.get_collider().hit()


func _on_explosion_timer_timeout():
	#print("Explosion finished")
	queue_free()

func on_body_entered_explosion_area(body):
	if body.has_method("hit"):
		#print("Body hit by explosion: " + str(body))
		body.call_deferred("hit")
