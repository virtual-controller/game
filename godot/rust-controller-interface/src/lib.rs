use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;
use godot::engine::web_socket_peer;
use godot::prelude::*;
use godot::engine::Node2D;
use godot::engine::INode2D;
use godot::builtin::Dictionary;
use godot::builtin::Array;
use rust_joystick_server::Libp2pMessageType;
use rust_joystick_server::PeerId;
use tokio::sync::mpsc;
use std::thread;
use ntex::web;
use ntex_files::NamedFile;
use std::path::{Path, PathBuf};
use std::format;
use tracing::{info, debug, trace};
use std::str::FromStr;
use std::io::{BufReader, Read, Write};
use std::fs::File;
use rustls::{Certificate, PrivateKey, ServerConfig};
use rustls_pemfile::{certs, rsa_private_keys};
use std::env;
use rcgen::generate_simple_self_signed;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}


#[derive(GodotClass)]
#[class(base=Node2D)]
struct ControllerInterface {
    inputs: Arc<Mutex<HashMap<PeerId, Vec<Libp2pMessageType>>>>,
    thread_handle: Option<thread::JoinHandle<()>>,
    base: Base<Node2D>,
}

#[godot_api]
impl INode2D for ControllerInterface {
    fn init(base: Base<Node2D>) -> Self {
        godot_print!("Hello, world2"); // Prints to the Godot console
        let map = HashMap::new();
        let map = Mutex::new(map);
        let map = Arc::new(map);

        Self {
            inputs: map,
            thread_handle: None,
            base,
        }
    }

    fn ready(&mut self) {
        godot_print!("Starting libp2p thread");

        let _ = tracing_subscriber::fmt()
            .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
            .try_init();

        let thread_inputs = self.inputs.clone();
        let t = thread::spawn( move || {
            info!("Started thread");
            ntex::rt::System::new("controller-web")
                .block_on(async move {
                    let (tx, mut rx) = mpsc::channel(10_000);
                    let task = rust_joystick_server::start_server(tx);
                    let task = tokio::spawn(task);
                    
                    let path = env::current_dir().unwrap();
                    info!("PWD: {:?}", path);
                    // TODO get address automatically
                    let subject_alt_names :&[_] = &["192.168.10.146".to_string(),
	"localhost".to_string()];
                    let cert = generate_simple_self_signed(subject_alt_names).unwrap();
                    let key_der = cert.serialize_private_key_der();
                    let key = rustls::PrivateKey(key_der);
                    let cert_der = cert.serialize_der().unwrap();
                    let cert_chain = vec![rustls::Certificate(cert_der)];
                    let config = rustls::ServerConfig::builder()
                        .with_safe_defaults()
                        .with_no_client_auth()
                        .with_single_cert(cert_chain, key)
                        .unwrap();
                    let web_server = web::HttpServer::new(|| web::App::new().route("/{filename}*", web::get().to(index)))
                        .bind(("0.0.0.0", 8080)).unwrap()
                        .run();
                    let web_server = ntex::rt::spawn(web_server);

                    loop {
                        debug!("Waiting on msg...");
                        let msg = rx.recv().await;
                        debug!("Got msg");
                        match msg {
                            Some(msg) => {
                                match msg {
                                    rust_joystick_server::ControllerMessage { peer, msg } => {
                                        debug!("Waiting on lock...");
                                        let mut inputs = thread_inputs.lock().await;
                                        debug!("Got lock");
                                        let v = inputs.entry(peer).or_insert(Vec::new());
                                        v.push(msg);
                                        debug!("Inserted message");
                                    },
                                }
                            },
                            None => break,
                        }
                    }
                });
        });

        self.thread_handle = Some(t);
    }
}

#[godot_api]
impl ControllerInterface {
    #[func]
    fn clear_player_input(&mut self) {
        debug!("Waiting on clear lock...");
        let mut map = self.inputs.blocking_lock();
        debug!("Got clear lock");
        map.clear();
    }

    #[func]
    fn clear_single_player_input(&mut self, peer: String) {
        let peer = PeerId::from_str(&peer).unwrap();
        debug!("Waiting on clear lock...");
        let mut map = self.inputs.blocking_lock();
        debug!("Got clear lock");
        map.remove(&peer);
    }

    #[func]
    fn get_last_player_input(&mut self) -> Array<Dictionary> {
        let mut map = self.inputs.blocking_lock();
        let mut dict_vec = Array::new();
        for (peer, v) in map.iter() {
            for msg in v.iter() { 
                let mut dict = Dictionary::new();
                match msg {
                    Libp2pMessageType::JoystickMessage(msg) => {
                        let mut joy_msg = Dictionary::new();
                        joy_msg.set("msg_type", "joystick");
                        joy_msg.set("x", msg.x);
                        joy_msg.set("y", msg.y);
                        joy_msg.set("direction", msg.direction.clone());
                        dict.set(peer.to_string(), joy_msg);
                    },
                    Libp2pMessageType::ButtonMessage(msg) => {
                        let mut but_msg = Dictionary::new();
                        but_msg.set("msg_type", "button");
                        but_msg.set("name", msg.name.clone());
                        but_msg.set("mode", msg.mode.to_string());
                        dict.set(peer.to_string(), but_msg);
                    },
                    Libp2pMessageType::Address(s, img) => {
                        let mut but_msg = Dictionary::new();
                        but_msg.set("msg_type", "address");
                        but_msg.set("address", s.clone());

                        use godot::engine::{image, Image};
                        let mut svgimg = Image::create(100, 100, false, image::Format::L8).unwrap();
                        let img = GString::from_str(img).unwrap();
                        svgimg.load_svg_from_string(img);
                        but_msg.set("qrcode", svgimg);
                        dict.set(peer.to_string(), but_msg);
                    },
                    Libp2pMessageType::Connected => {
                        let mut but_msg = Dictionary::new();
                        but_msg.set("msg_type", "connected");
                        dict.set(peer.to_string(), but_msg);
                    },
                    Libp2pMessageType::Disconnected => {
                        let mut but_msg = Dictionary::new();
                        but_msg.set("msg_type", "disconnected");
                        dict.set(peer.to_string(), but_msg);
                    },
                };
                dict_vec.push(dict);
            }
        }
        map.clear();
        dict_vec
    }
}



async fn index(req: web::HttpRequest) -> Result<NamedFile, web::Error> {
    let path: PathBuf = req.match_info().query("filename").parse().unwrap();
    let path: PathBuf = Path::new("./dist").join(path);
    Ok(NamedFile::open(path)?)
}

