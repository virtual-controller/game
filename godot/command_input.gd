extends Node2D

signal command_output

var SPRITES = []
var LAST_DIRECTION = "C"
var COMMAND_SEQUENCE = []

var REVIVE_SEQUENCE = ["N", "S", "E", "W", "E", "N"]
var LASER_SEQUENCE = ["N", "S", "N", "S"]
var BOMB_SEQUENCE = ["W", "N", "E", "S", "N"]
var ADVANCE_LEVEL = ["N", "E", "S", "W", "N", "E"]

func clear_sprites():
	for sprite in SPRITES:
		sprite.queue_free()
	SPRITES.clear()

func add_sprite(texture):
	var sprite = Sprite2D.new()
	sprite.texture = texture
	sprite.scale = Vector2(0.06, 0.06)
	sprite.offset = Vector2(SPRITES.size() * 450, 0)
	SPRITES.push_back(sprite)
	add_child(sprite)

func _on_player_command_press(input):
	if input['msg_type'] == 'command_end':
		clear_sprites()
		COMMAND_SEQUENCE.clear()
	elif LAST_DIRECTION != input['direction'] and input['direction'] != "C":
		LAST_DIRECTION = input['direction']

		var texture;
		if LAST_DIRECTION == "N":
			texture = load("res://resources/arrow-sm-up-svgrepo-com.svg")
		elif LAST_DIRECTION == "W":
			texture = load("res://resources/arrow-sm-left-svgrepo-com.svg")
		elif LAST_DIRECTION == "S":
			texture = load("res://resources/arrow-sm-down-svgrepo-com.svg")
		elif LAST_DIRECTION == "E":
			texture = load("res://resources/arrow-sm-right-svgrepo-com.svg")
		add_sprite(texture)
		COMMAND_SEQUENCE.push_back(LAST_DIRECTION)

		if COMMAND_SEQUENCE.size() >= 4:
			var correct_sequence = false
			if COMMAND_SEQUENCE == REVIVE_SEQUENCE:
				command_output.emit("REVIVE")
				correct_sequence = true
			elif COMMAND_SEQUENCE == LASER_SEQUENCE:
				command_output.emit("LASER")
				correct_sequence = true
			elif COMMAND_SEQUENCE == BOMB_SEQUENCE:
				command_output.emit("BOMB")
				correct_sequence = true
			elif COMMAND_SEQUENCE == ADVANCE_LEVEL:
				command_output.emit("ADVANCE_LEVEL")
				correct_sequence = true
			
			if correct_sequence == true:
				clear_sprites()
				texture = load("res://resources/check-svgrepo-com.svg")
				add_sprite(texture)
				COMMAND_SEQUENCE.clear()
		if COMMAND_SEQUENCE.size() >= 6:
			clear_sprites()
			texture = load("res://resources/cross-svgrepo-com.svg")
			add_sprite(texture)
			COMMAND_SEQUENCE.clear()
