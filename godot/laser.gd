extends CharacterBody2D

var IS_ITEM = false
var IS_SHOOTING = false
var LASER_TIME = 0.5
var LASER_SPRITE = null
var PLAYER = null

# item
func hit():
	if IS_ITEM == true:
		queue_free()
	
func spawn_item_in_area(area):
	var spawnArea = area.shape.extents
	var origin = area.global_position
	var x = randf_range(origin.x - spawnArea.x, origin.x + spawnArea.x)
	var y = randf_range(origin.y - spawnArea.y, origin.y + spawnArea.y)
	var pos = Vector2(x, y)
	spawn_item(pos)

func spawn_item(pos):
	IS_ITEM = true
	position = pos

func shoot(pos, rot, player):
	if IS_SHOOTING == false:
		PLAYER = player
		velocity = Vector2.ZERO
		IS_SHOOTING = true
		#var timer = $LaserTimer
		$LaserTimer.start()
		$LaserTimer.wait_time = LASER_TIME
		$Sprite2D.visible = false
		
		# Explosion texture
		var laser_sprite = Sprite2D.new()
		var texture = GradientTexture2D.new()
		texture.fill = GradientTexture2D.FILL_LINEAR
		texture.width = 25
		texture.height = 1000
		var gradient = Gradient.new()
		var blue = Color(float(0xa6)/255.0, float(0xc1)/255.0, float(0xff)/255.0)  # XXX Got values manually
		var blue_invis = Color(blue, 0)
		# XXX There are default ones apparently
		gradient.set_color(0, blue_invis)
		gradient.set_color(1, blue_invis)
		gradient.add_point(0.5, blue)
		texture.gradient = gradient
		laser_sprite.texture = texture
		LASER_SPRITE = laser_sprite
		
		var area = Area2D.new()
		var col = CollisionShape2D.new()
		var shape = RectangleShape2D.new()
		shape.size = Vector2(25, 1000)
		col.shape = shape
		
		area.position = pos + Vector2(500,0).rotated(rot)  # TODO grab from shape
		area.rotation = rot + PI/2
		area.add_child(col)
		area.add_child(laser_sprite)
		area.body_entered.connect(on_laser_area_enter)
		add_child(area)

func _process(delta):
	if IS_SHOOTING:
		var fade_percent = $LaserTimer.time_left / LASER_TIME
		var color = LASER_SPRITE.texture.gradient.get_color(1)
		color.a = fade_percent
		LASER_SPRITE.texture.gradient.set_color(1, color)

func _physics_process(delta):
	if IS_SHOOTING == false:
		var collision = move_and_collide(velocity*delta)
		if collision:
			if IS_ITEM == true && collision.get_collider().has_method("pickup_item"):
				collision.get_collider().pickup_item("LASER")
				queue_free()




func on_laser_area_enter(body):
	#print("Pew pew lazerbeams: " + str(body))
	if body != PLAYER:
		if body.has_method("hit"):
			#print("Body hit by laser: " + str(body))
			body.call_deferred("hit")
	


func _on_laser_timer_timeout():
	if IS_SHOOTING == true:
		queue_free()
