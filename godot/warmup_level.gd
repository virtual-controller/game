extends Node2D

signal player_finished

func get_spawn_areas():
	var d = {
		'start': $StartArea/CollisionShape2D,
	}
	return d


func _on_finish_area_body_entered(body):
	player_finished.emit(body)
	body.queue_free()
