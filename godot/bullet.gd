extends CharacterBody2D


const SPEED = 700.0

func start(_position, _direction, player_speed):
	rotation = _direction
	position = _position
	velocity = Vector2(SPEED, 0).rotated(rotation) + player_speed

func _physics_process(delta):
	var collision = move_and_collide(velocity*delta)
	if collision:
		# TODO make it so you cant collide with your own bulets
		#print("PLAYER: " + str(PLAYER) + " collider: " + str(collision.get_collider()))
		#if collision.get_collider() == PLAYER:
		#	pass
		if collision.get_collider().has_method("hit"):
			collision.get_collider().hit()
		queue_free()
