extends Node2D

@export var player_scene: PackedScene

var QRCODE_SPRITE = null
var MAP = null
var SPAWN_AREAS = {}
var MODE = "JOIN" # JOIN WARMUP MATCH
# Called when the node enters the scene tree for the first time.
func _ready():
	MAP = load("res://join_map.tscn").instantiate()
	SPAWN_AREAS = MAP.get_spawn_areas()
	add_child(MAP)

func player_color_from_peer_name(peer):
	var color = Color.WHITE
	var peer_int = int(peer)
	color.r = float((peer_int & 0xFF0000) >> 16) / 256
	color.g = float((peer_int & 0x00FF00) >> 8) / 256
	color.b = float((peer_int & 0x0000FF) >> 0) / 256
	return color

func team_color_from_peer_name(color, peer):
	var peer_int = int(peer)
	var r = float((peer_int & 0xFF0000) >> 16) / 1024
	var g = float((peer_int & 0x00FF00) >> 8) / 1024
	var b = float((peer_int & 0x0000FF) >> 0) / 1024
	var color_r = color.r
	var color_g = color.g
	var color_b = color.b
	if peer_int % 2 == 1:
		color.r += r
	else:
		color.r -= r
	if (peer_int >> 8) % 2 == 1:
		color.g += g
	else:
		color.g -= g
	if (peer_int >> 16) % 2 == 1:
		color.b += b
	else:
		color.b -= b
	return color

var players = {}
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var ci = $ControllerInterface;
	var inputs = ci.get_last_player_input()  # Vec<Dict<Peer, Msg>>
	if inputs.size() > 0:
		for dict in inputs:
			for peer in dict:
				var input = dict[peer]
				match input:
					{"msg_type": "address", "address": var address, "qrcode": var qrcode}:
						#print("Got an address: " + str(address) + "\n" + str(qrcode))
						var texture = ImageTexture.create_from_image(qrcode)
						var qrcode_sprite = Sprite2D.new()
						qrcode_sprite.texture = texture
						qrcode_sprite.scale = Vector2(0.5, 0.5)
						qrcode_sprite.offset = Vector2(200, 200)
						QRCODE_SPRITE = qrcode_sprite
						add_child(qrcode_sprite)
						ci.clear_single_player_input(peer)
					{"msg_type": "button", "name": var button_name, "mode": var mode}:
						#print("Got a button message")
						if peer not in players:
							continue
						var player = players[peer]
						player.control_input.emit(input)
					{"msg_type": "joystick", "x": var x, "y": var y, "direction": var direction}:
						#print("Got a joystick message")
						if peer not in players:
							continue
						var player = players[peer]
						player.control_input.emit(input)
					{"msg_type": "connected"}:
						print("Got a connected message: " + peer)
						if MODE == "JOIN":
							var new_player = player_scene.instantiate()
							new_player.PEERID = peer
							new_player.command_output.connect(on_player_command)
							#var color = player_color_from_peer_name(peer)
							var color = team_color_from_peer_name(Color.DARK_GREEN, peer)
							new_player.set_color(color)
							players[peer] = new_player
							new_player.IS_DEAD = true
							#if SPAWN_AREAS.has('item'):
							#	var item_spawn_area = SPAWN_AREAS['item']
							#	new_player.spawn_ship_in_area(item_spawn_area)
							add_child(new_player)
						ci.clear_single_player_input(peer)
					{"msg_type": "disconnected"}:
						print("Got a disconnect message: " + peer)
						if peer not in players:
							continue
						var player = players[peer]
						players.erase(peer)
						player.queue_free()
						ci.clear_single_player_input(peer)
					_:
						print("Got something else")

func _process(delta):
	match MODE:
		"JOIN":
			for peer in players:
				var player = players[peer]
				if player.is_dead():
					if SPAWN_AREAS.has('item'):
						var item_spawn_area = SPAWN_AREAS['item']
						player.spawn_ship_in_area(item_spawn_area)
		"WARMUP":
			pass
		"MATCH":
			pass


func on_player_command(command, p):
	print("Command: " + str(command))
	var item_spawn_area = SPAWN_AREAS['item']
	match command:
		"REVIVE":
			# TODO how to handle teams?
			for player_str in players:
				var player = players[player_str]
				if p.TEAM == player.TEAM && player.is_dead():
					player.spawn_ship_in_area(item_spawn_area)
		"LASER":
			var laser_scene = load("res://laser.tscn")
			var l = laser_scene.instantiate()
			l.spawn_item_in_area(item_spawn_area)
			get_tree().root.add_child(l)
		"BOMB":
			var bomb_scene = load("res://bomb.tscn")
			var b = bomb_scene.instantiate()
			b.spawn_item_in_area(item_spawn_area)
			get_tree().root.add_child(b)
		"ADVANCE_LEVEL":
			if MODE == "JOIN":
				QRCODE_SPRITE.queue_free()
				var map = load("res://warmup_level.tscn").instantiate()
				MAP.queue_free()
				MAP = map
				SPAWN_AREAS = map.get_spawn_areas()
				map.player_finished.connect(on_warmup_player_finish)
				for peer in players:
					var player = players[peer]
					player.clear()
					player.spawn_ship_in_area(SPAWN_AREAS['start'])
				add_child(map)

var warmup_finished_bodies = []
func on_warmup_player_finish(body):
	print("Player finished: " + str(body))
	warmup_finished_bodies.push_back(body)
	var peer2 = body.get_peerid()
	if warmup_finished_bodies.size() >= players.size():
		MODE = "MATCH"
		var map = load("res://pacman_level.tscn").instantiate()
		MAP.queue_free()
		MAP = map
		SPAWN_AREAS = map.get_spawn_areas()

		for peer in players:
			var player = players[peer]
			player.clear()
			player.spawn_ship_in_area(SPAWN_AREAS['item'])
		add_child(map)
