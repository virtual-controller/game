extends Node2D

@export var ship_scene: PackedScene
@export var fetus_scene: PackedScene

#signal hit
signal control_input
signal command_press
signal command_output

var LAST_INPUT = {'joystick': {'x': 0, 'y': 0},
	'a_button': {'mode': 'Up'},
	'b_button': {'mode': 'Up'},
	'command_button': {'mode': 'Up'},
}
var PRESSING_A = false
var PRESS_A_NEXT_FRAME = false;
var PRESSING_B = false
var PRESS_B_NEXT_FRAME = false;
var PRESSING_COMMAND = false;
var PRESS_COMMAND_NEXT_FRAME = false;
var IS_FETUS = false
var IS_DEAD = false
var SPAWN_POINT = null
var MAX_BULLET_COUNT = 3
var BULLET_COUNT = 3
var COMMAND_LIST = []
var SHIP = null
var FETUS = null
var PLAYER_COLOR = Color.WHITE
var TEAM = null
var PEERID = ""

func shoot():
	print("Bullet Count: " + str(BULLET_COUNT))
	if BULLET_COUNT < MAX_BULLET_COUNT and $BulletRespawnTimer.is_stopped():
		$BulletRespawnTimer.start()
	
	if BULLET_COUNT > 0 && SHIP != null:
		SHIP.shoot()
		BULLET_COUNT -= 1

func shoot_special():
	if SHIP != null:
		SHIP.shoot_special()


func spawn_ship_at_point(point):
	IS_DEAD = false
	var rot = 0.0
	if FETUS != null:
		rot = FETUS.get_rotation()
		FETUS.queue_free()
	if SHIP == null:
		SHIP = ship_scene.instantiate()
		SHIP.PEERID = PEERID
		SHIP.was_hit.connect(hit)
		SHIP.set_color(PLAYER_COLOR)
		add_child(SHIP)
	PhysicsServer2D.body_set_state(
		SHIP.get_rid(),
		PhysicsServer2D.BODY_STATE_TRANSFORM,
		Transform2D.IDENTITY.translated(point).rotated(rot)
	)
	SHIP.position = point

func spawn_ship(spawn_point = null):
	if spawn_point != null:
		SPAWN_POINT = spawn_point
	spawn_ship_at_point(SPAWN_POINT.position)

func spawn_ship_in_area(area):
	var spawnArea = area.shape.extents
	var origin = area.global_position
	var x = randf_range(origin.x - spawnArea.x, origin.x + spawnArea.x)
	var y = randf_range(origin.y - spawnArea.y, origin.y + spawnArea.y)
	var pos = Vector2(x, y)
	spawn_ship_at_point(pos)

func clear():
	if SHIP != null:
		SHIP.queue_free()
		SHIP = null
	if FETUS != null:
		FETUS.queue_free()
		FETUS = null

func hit():
	if SHIP != null:
		$ShipRespawnTimer.start()

		var ship_pos = SHIP.position
		var ship_vel = SHIP.linear_velocity
		var player_rotation = transform.get_rotation()
		# TODO also rotate the fetus
		SHIP.queue_free()
		SHIP = null
		FETUS = fetus_scene.instantiate()
		PhysicsServer2D.body_set_state(
			FETUS.get_rid(),
			PhysicsServer2D.BODY_STATE_TRANSFORM,
			Transform2D.IDENTITY.translated(ship_pos).rotated(player_rotation)
		)
		FETUS.position = ship_pos
		PhysicsServer2D.body_set_state(
			FETUS.get_rid(),
			PhysicsServer2D.BODY_STATE_LINEAR_VELOCITY,
			ship_vel
		)
		FETUS.set_color(PLAYER_COLOR)
		FETUS.was_hit.connect(hit)
		add_child(FETUS)
	elif FETUS != null:
		FETUS.queue_free()
		IS_DEAD = true

func set_color(color):
	#print("setting color: " +  str(color))
	PLAYER_COLOR = color
	if SHIP != null:
		SHIP.set_color(color)
	if FETUS != null:
		FETUS.set_color(color)

func is_dead():
	return IS_DEAD

func _process(delta):
	if PRESS_A_NEXT_FRAME or (LAST_INPUT['a_button']['mode'] == 'Down' and PRESSING_A == false):
		PRESS_A_NEXT_FRAME = false
		PRESSING_A = true
		shoot()
	if LAST_INPUT['a_button']['mode'] == 'Up' and PRESSING_A == true:
		PRESSING_A = false
	
	if PRESS_B_NEXT_FRAME or (LAST_INPUT['b_button']['mode'] == 'Down' and PRESSING_B == false):
		PRESS_B_NEXT_FRAME = false
		PRESSING_B = true
		shoot_special()
	if LAST_INPUT['b_button']['mode'] == 'Up' and PRESSING_B == true:
		PRESSING_B = false
	
	if PRESS_COMMAND_NEXT_FRAME or (LAST_INPUT['command_button']['mode'] == 'Down' and PRESSING_COMMAND == false):
		PRESS_COMMAND_NEXT_FRAME = false
		PRESSING_COMMAND = true
		if SHIP != null:
			SHIP.PRESSING_COMMAND = true
	if LAST_INPUT['command_button']['mode'] == 'Up' and PRESSING_COMMAND == true:
		PRESSING_COMMAND = false
		if SHIP != null:
			SHIP.PRESSING_COMMAND = false
			var msg = {'msg_type': 'command_end'}
			command_press.emit(msg)
	
	if SHIP != null:
		$Arrows.position = SHIP.position
		$Arrows.position.y -= 30





func _on_control_input(input):
	if IS_DEAD == true:
		return
	if 'msg_type' in input and input['msg_type'] == 'joystick':
		LAST_INPUT['joystick'] = input
		if PRESSING_COMMAND == true && SHIP != null:
			command_press.emit(input)
	elif 'msg_type' in input and input['msg_type'] == 'button':
		if input['name'] == 'A':
			LAST_INPUT['a_button'] = input
			if input['mode'] == 'Down':
				PRESS_A_NEXT_FRAME = true
		elif input['name'] == 'B':
			LAST_INPUT['b_button'] = input
			if input['mode'] == 'Down':
				PRESS_B_NEXT_FRAME = true
		elif input['name'] == 'COMMAND':
			LAST_INPUT['command_button'] = input
			if input['mode'] == 'Down':
				PRESS_COMMAND_NEXT_FRAME = true
	if SHIP != null:
		SHIP.set_last_input(LAST_INPUT)
	elif FETUS != null:
		FETUS.set_last_input(LAST_INPUT)


func _on_bullet_respawn_timer_timeout():
	if BULLET_COUNT == MAX_BULLET_COUNT:
		$BulletRespawnTimer.stop()
	else:
		BULLET_COUNT += 1



func _on_command_output(command):
	#print("command: " + str(command))
	command_output.emit(command, self)



func _on_ship_respawn_timer_timeout():
	if FETUS != null:
		var position = FETUS.position
		spawn_ship_at_point(position)
